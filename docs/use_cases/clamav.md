# Protecció del sistema amb l'antivirus ClamAV

![Imatge ClamAV](clamav_assets/clamav.webp){style="height:25%;width:25%" align=left}

[ClamAV](https://www.clamav.net) és un software multiplataforma, gratuït i "open-source" (tot i que desenvolupat principalment per l'empresa Cisco) que permet detectar i reconèixer diferents tipus d'amenaces pel nostre sistema (com ara virus, troians, adware, rootkits i malware en general) gràcies a mantenir actualitzada a diari una gran base de dades pròpia on aquests elements maliciosos s'hi troben llistats. 

L'objectiu d'aquesta pràctica és familiaritzar-se amb la configuració i el funcionament d'aquesta eina per tal d'aprendre com utilitzar-la en el dia a dia


## Cicles, Mòduls i UFs on es pot realitzar aquesta pràctica

* **SMX  MP06** UF4:Seguretat activa
* **ASIX MP05** UF2:Instal·lació, configuració i recuperació de programari
* **ASIX MP11** UF2:Seguretat activa i accés remot


## Infraestructura prèvia necessària per implementar aquesta pràctica

!!! warning "Vigila l'ús de vCPUs i de RAM!"

    És **molt important** que la quantitat de vCPUs i de memòria RAM assignades a les màquines utilitzades en aquesta pràctica sigui el més adjustada possible!!
    
    La quantitat global d'hores que un centre educatiu té disponible per utilitzar IsardVDI (les anomenades "DHN", de "Desktop Hours Number") ve donada per la següent [fòrmula](https://isard.gitlab.io/isardvdi-docs-pilot-fp/usage/usage.ca/#__tabbed_1_1). Això implica que quanta més vCPUs i -sobre tot- RAM s'assignin a les màquines creades dins d'un centre concret, menys hores d'Isard aquest centre podrà consumir en total.

!!! info "Idees per no consumir gaire RAM"

    * Utilitzar distribucions basades en Linux de tipus "Server" (és a dir, sense entorn gràfic). Exemples de distribucions d'aquest tipus són, per exemple, [Debian Server](https://www.debian.org), [Ubuntu Server](https://ubuntu.com/download/server), [Fedora Server](https://fedoraproject.org/server), [openSUSE Server](https://get.opensuse.org/server), [Rocky Linux Server](https://rockylinux.org) o [Alma Linux Server](https://almalinux.org), entre moltes altres. 
    
    * Utilitzar distribucions que, tot i tenir un entorn gràfic, aquest sigui més minimalista que els entorns estàndard (com GNOME o KDE) i, per tant, més lleuger de recursos. Exemples de distribucions d'aquest tipus són: [Slitaz](https://www.slitaz.org), [Puppy](https://puppylinux-woof-ce.github.io), [Slax](https://www.slax.org), [Bodhi](https://www.bodhilinux.com), [Lubuntu](https://lubuntu.me), [Fedora LXQT](https://fedoraproject.org/spins/lxqt) o fins i tot [Fedora Sway](https://fedoraproject.org/spins/sway), entre moltes altres
       
    En tot cas, si a l'hora de voler crear un escriptori s'optés per triar alguna de les distribucions anteriors, caldria comprovar primer que IsardVDI ofereix la plantilla-base corresponent; si no és així, llavors caldria realitzar manualment la instal·lació de l'escriptori desitjat a partir del medi ISO adient (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/media)). 
    
    Si aquest sistema instal·lat es volgués utilitzar, a més, per fer un desplegament, llavors caldria primer crear a partir de l'escriptori anterior una plantilla pròpia (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/templates/#create)) i tot seguit fer el desplegament a partir d'ella (tal com s'explica [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/deployments)).

!!! tip "Utilitza el visor SPICE per millorar la teva experiència d'usuari"

    A l'hora de triar un visor per a les teves màquines, et recomanem que utilitzis el visor SPICE ja que ofereix, a més de molt poca latència, una major funcionalitat i flexibilitat que els altres visors disponibles. Per exemple, mitjançant aquest visor podràs gaudir de l'àudio integrat i seràs capaç de copiar-pegar entre la teva màquina real local i les màquines Isard (si no són de tipus "Server"), així com també connectar-hi dispositius USB en la primera i que siguin visibles en les segones, entre altres possibilitats. Per saber més informació sobre les característiques dels diferents visors disponibles, pots consultar [el manual](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers)


Per realitzar aquesta pràctica només es necessitarà disposar d'una màquina virtual, la qual pot consistir en un desplegament ofert pel professor a tot el grup-clase o bé un escriptori generat per cada alumne individualment, tant se val. A continuació es detallen els passos a fer en qualsevol d'aquestes dues opcions.

!!! example "Creació de la màquina de treball"

    === "En forma de desplegament"
    
        1. Triar l'opció "Desplegaments del menú superior i tot seguit pitjar en el botó "Desplegament nou"
        2. Triar, al formulari web que apareix, les característiques que desitjem que tingui la màquina. En destacarem les que són rellevants per aquesta pràctica:
            + **Visible**: SÍ (caldrà indicar a més, en l'apartat corresponent, el grup i/o alumnes concrets als quals se'ls vol mostrar la màquina)
            + **vCPUs** i **RAM**: És important tenir en compte el que s'indica al primer quadre d'aquesta pàgina  
            + **Plantilla base**: Qualsevol de tipus Linux (Debian, Ubuntu, Fedora). És important tenir en compte, però, el que s'indica al segon quadre d'aquesta pàgina  
            + **Visors**: És important tenir en compte el que s'indica al tercer quadre d'aquesta pàgina  
        3. Pitjar el botó "Crear". 
        
        En tot cas, teniu més informació sobre com crear desplegaments i les possibilitats que aquests ofereixen [aquí](https://isard.gitlab.io/isardvdi-docs/advanced/deployments)
    
    === "En forma d'escriptori"
    
        1. Pitjar en el botó "Escriptori nou"
        2. Triar, al formulari web que apareix, les característiques que desitjem que tingui la màquina. En destacarem les que són rellevants per aquesta pràctica:
            + **vCPUs** i **RAM**: És important tenir en compte el que s'indica al primer quadre d'aquesta pàgina  
            + **Plantilla base**: Qualsevol de tipus Linux (Debian, Ubuntu, Fedora). És important tenir en compte, però, el que s'indica al segon quadre d'aquesta pàgina 
            + **Visors**: És important tenir en compte el que s'indica al tercer quadre d'aquesta pàgina    
        3. Pitjar el botó "Crear".     
        
        En tot cas, teniu més informació sobre com crear escriptoris i les possibilitats que aquests ofereixen [aquí](https://isard.gitlab.io/isardvdi-docs/user/create_desktop)


## Enunciat de la pràctica

### Instal·lació del software ClamAV

Els paquets a instal·lar s'anomenen de forma diferent depenent de la distribució de Linux triada. En concret:

!!! quote ""

    === "Fedora"

        ```bash
        sudo dnf install clamav clamd clamav-update
        ```

    === "Ubuntu/Debian"

        ```bash
        sudo apt install clamav clamav-daemon clamav-freshclam
        ```

ClamAV té una arquitectura interna de tipus client-servidor. En aquest sentit, cal saber que el paquet *"clamav"* de Fedora inclou, a més de les comandes de terminal que de seguida veurem, totes les definicions dels diferents serveis Systemd de ClamAV funcionant com a client, però el servei clamd específicament (és a dir, "la part servidora") és proporcionat per separat, en el paquet *"clamd"*. A Ubuntu/Debian, en canvi, el paquet *"clamav"* només inclou comandes de terminal mentre que tots els serveis (tant els de client com el de servidor) estan disponibles en el paquet *"clamav-daemon"*

D'altra banda, el paquet *"clamav-data"* (instal·lat automàticament com a dependència del paquet *"clamav"*, tant a Ubuntu/Debian com a Fedora) conté la base de dades inicial de signatures de malware (és a dir, el conjunt de hashes de fitxers i de patrons binaris o de cadena que identifiquen inequívocament cada peça de software maliciós conegut) sobre la que ClamAV intentarà trobar-hi coincidències amb els fitxers presents al sistema quan faci els escanejos del disc; aquesta base de dades s'actualitza regularment i de forma automàtica (comunicant-se amb un servidor central ubicat a Internet i proporcionat per Cisco gratuïtament) gràcies a una tasca programada que, atenció, estarà present al sistema només si s'instal·la el paquet *"clamav-update"* (a Fedora) o *"clamav-freshclam"* (a Ubuntu/Debian)

!!! note "Alternatives"

    A sistemes Windows tenim la possibilitat d'instal·lar una "frontend" gràfic de ClamAV anomenat [ClamWin](https://clamwin.com), el qual permet realitzar totes les tasques que descriurem en aquesta pràctica des de finestres. D'altra banda, també cal mencionar l'existència de l'eina [LMD](https://www.rfxn.com/projects/linux-malware-detect), alternativa també lliure i gratuïta a ClamAV que pot fer-se servir com a complement.
    

### Actualització inicial de la base de dades de signatures

Un cop instal·lat ClamAV, el primer que hauríem de fer és descarregar del servidor central d'Internet la base de dades de signatures de malware més actualitzada que hi hagi en aquest precís moment (ja que el paquet *"clamav-data"* instal·lat de sèrie és segur que no estarà al dia). Depenent de la distribució de Linux triada, això s'aconsegueix de formes diferents:
	
!!! quote ""

    === "Fedora"	
	
        Per realitzar l'actualització manual de la base de dades de signatures hem d'executar cada cop la següent comanda:
	
        ```bash
        sudo freshclam
        ```

        En el cas d'haver instal·lat el paquet *"clamav-update"*, aquesta actualització aconseguirem que sigui automàtica (per defecte, cada dues hores) si iniciem i activem el servei homònim, així:

        ```bash
        sudo systemctl --now enable clamav-freshclam
        ```

    === "Ubuntu/Debian"
    
        En instal·lar-se el paquet *"clamav-freshclam"* (el qual, de fet, si no s'instal·la explícitament és instal·lat automàticament com a dependència del paquet *"clamav"*), a diferència del que passava a Fedora, el servei homònim és posat en marxa automàticament (i és activat per propers reinicis del sistema també!). Per tant, com que degut a l'execució immediata del servei "clamav-freshclam" l'actualització de les signatures es fa sempre just en instal·lar ClamAV (i, a partir d'aquí, cada dues hores), en principi no caldrà fer mai l'actualització manualment (executant la comanda ```sudo freshclam``` explícitament) ni ens haurem de preocupar d'aquest tema.
    
En tot cas, aquesta base de dades descarregada es guardarà a la carpeta "/var/lib/clamav" i estarà formada per tres fitxers d'un format comprimit i signat digitalment anomenat "CVD" (de "ClamAV Virus Database")...:

* **"daily.cvd"** : Conté les signatures MD5/SHA256 dels fitxers maliciosos més recents (s'actualitza diàriament)
* **"main.cvd"** : Conté les signatures MD5/SHA256 que prèviament hi eren al fitxer "daily.cvd" i que tenen un risc baix de falsos positius
* **"bytecode.cvd"** : Conté el conjunt de patrons binaris maliciosos compilats 

...els quals poden ser, si cal, inspeccionats i manipulats internament amb una comanda específica (i avançada) anomenada sigtool. Per exemple, per saber, entre altres dades estadístiques, la quantitat de signatures incloses en algun dels fitxers anteriors (per exemple, "main.cvd"), es pot executar la següent comanda concreta (per veure'n més possibilitats, consulta ```man sigtool``` i també la [documentació oficial online del programa](https://docs.clamav.net/manual/Signatures.html)):

```bash
sigtool -i /var/lib/clamav/main.cvd
```

D'altra banda, la comanda freshclam (així com el servei clamav-freshclam, el qual en essència executa aquesta comanda en segon pla) té associat com a fitxer de configuració el fitxer **"/etc/freshclam.conf"** . D'entre totes les directives que allà hi són presents, en podem destacar les següents (pots trobar més informació a ```man freshclam.conf```)

*    *DatabaseMirror* : Indica el nom DNS del servidor central d'Internet d'on s'obtindran les actualitzacions de la base de dades de signatures. Per defecte és "database.clamav.net". D'altra banda, es poden afegir URLs "ad-hoc" (de tipus "http://", "https://" o "file://") apuntant a altres fitxers de signatures alternatius amb la directiva *DatabaseCustomURL*
*    *Checks* : Indica el nombre de comprovacions que realitzarà diàriament el servei clamav-freshclam contra el servidor central d'Internet especificat a la directiva anterior per tal de mantenir permanentment actualitzada la base de dades de signatures local. Per defecte la freqüència de comprovacions és de dues hores.
*    *DatabaseDirectory* : Indica la ruta de la carpeta on es guardarà la base de dades de signatures. Per defecte és "/var/lib/clamav"


### Modes de treball de ClamAV

ClamAV és un software que actua sota demanda. És a dir, no és com altres antivirus que es mantenen en execució contínuament escanejant els eventuals fitxers que hi puguin aparèixer al disc, sinó que només s'executa quan l'usuari li demana, i només per fer un escaneig del contingut d'una carpeta (o d'un fitxer) especificada en concret (una excepció a aquest funcionament, cal dir-ho, és el dimoni *clamonacc*, però aquest és un nivell més avançat d'ús del programa). En aquest sentit, a ClamAV el podem fer servir bàsicament de dues maneres (sempre fent servir com a referència la base de dades de signatures actualitzada en el punt anterior):

#### Mode "standalone"

Podem realitzar directament un escaneig del fitxer o de la carpeta indicada (opcionalment, de forma recursiva) executant la comanda següent (cal tenir en compte, atenció, que depenent de si es necessiten permisos de lectura en el/s fitxer/s o carpeta/es indicats, aquesta comanda s'haurà d'executar com administrador o no): 

```clamscan /ruta/fitxerOcarpeta``` 

A la comanda anterior se li poden afegir diverses opcions (a escriure entre el nom de la comanda i la ruta), d'entre les quals en podem destacar les següents (per conéixer-ne més, consulta ```man clamscan```): 

<table><tr>
<td> <b>-r</b> </td><td> Quan s'indica una carpeta a escanejar (o vàries, si s'indiquen comodins), aquest paràmetre fa que l'escaneig es faci de forma recursiva (és a dir, que es faci no només pel contingut inmediat de la carpeta en qüestió sinó també pel contingut de totes les subcarpetes que hi puguin haver a dins d'ella).</td>
</tr><tr>
<td> <b>-i</b> </td><td> Mostra a la pantalla les rutes dels arxius infectats. Per defecte, si no s'indica aquest paràmetre (o el paràmetre <b>-o</b> , el qual fa que no es mostrin els missatges de fitxers "OK"), es mostren les rutes de tots els fitxers escanejats, una per una. Al final, a més, es mostrarà (a no ser que s'indiqui <b> --no-summary</b>) un resum estadístic final de tot l'escaneig. </td>
</tr><tr>
<td> <b>--remove</b> </td><td> Elimina els fitxers que s'hagin trobat infectats (per defecte no es toquen) </td>
</tr><tr>
<td> <b>--move /ruta/carpeta</b> </td><td> Indica la carpeta on es mouran els fitxers que s'hagin trobat infectats (si no s'indica aquest paràmetre, no es mouran). També existeix el paràmetre <b>--copy /ruta/carpeta</b>, que fa una còpia. </td>
</tr><tr>
<td> <b>--include "expr.reg"</b> </td><td> Indica una expressió regular que es confrontarà amb els noms dels fitxers ubicats sota la carpeta indicada per escanejar; només els fitxers el nom del qual hi coincideixi s'escanejaran, la resta no. També existeix el paràmetre contrari, <b>--exclude "expr.reg"</b>, el qual que fa que els fitxers que tinguin un nom concordant amb l'expressió regular indicada no siguin escanejats. Per exemple, *clamscan --include ".*\.txt$" -r /opt* només escaneja els fitxers sota la carpeta "/opt" (i subcarpetes) que tinguin l'extensió ".txt". </td>
</tr><tr>
<td> <b>-d /ruta/fitxerOCarpeta</b> </td><td> Indica explícitament un fitxer que es farà servir com a base de dades de signatures durant l'escaneig, o bé una carpeta (on podran estar ubicats múltiples fitxers). Per defecte s'utilitza la carpeta "/var/lib/clamav". </td>
</tr><tr>
<td> <b>-l /ruta/fitxer.log</b> </td><td> Guarda en el fitxer indicat el resum estadístic de l'escaneig. </td>
</tr><tr>
<td> <b>-f /ruta/fitxer</b> </td><td> En lloc d'indicar directament el/s fitxer/s o carpeta/es a escanejar com a paràmetres de la comanda, es pot utilitzar aquest paràmetre, el qual serveix per indicar un fitxer el contingut del qual serà la llista de rutes de fitxers o carpetes que seran escanejades (una per línia). </td>
</tr></table>

#### Mode client-servidor

Aquest mode és molt utilitzat per escanejar virus als missatges de correu, on el servidor de correu en aquest cas actuaria com a "client ClamAV" en sol·licitar al "servidor ClamAV" l'escaneig dels missatges que rep. En concret, per posar en marxa el "servidor ClamAV", només cal posar en marxa el dimoni pertinent, així:

```sudo systemctl --now enable clamd@scan``` (a Fedora) 

```sudo systemctl --now enable clamav-daemon``` (a Ubuntu/Debian)

Aquest dimoni no escaneja res en temps real (ni de forma programada) sinó que només ho fa quan és disparat mitjançant la comanda client següent (i només per realitzar la inspecció del/s fitxer/s o carpeta/es indicat/s en dita comanda client):

```clamdscan /ruta/fitxerOCarpeta``` 

Fent-ho així, la comanda client només aporta el fitxer/carpeta al servidor, que és qui realitza l'escaneig pròpiament dit; en aquest sentit, el servidor podria estar funcionant a la mateixa màquina local que el client o bé remotament; si fos aquest el cas, el client faria una "pujada" del/s fitxer/s en qüestió al servidor, el servidor faria tot seguit l'escaneig pròpiament dit i finalment retornaria el resultat d'aquest al client. 

A la comanda client anterior se li poden afegir diverses opcions (a escriure entre el nom de la comanda i la ruta), d'entre les quals en podem destacar les següents (per conéixer-ne més, consulta ```man clamdscan```, però cal tenir en compte que la majoria dels paràmetres de clamdscan no tenen efecte pràctic sobre la manera de com fer l'escaneig perquè aquest és realitzat realment pel dimoni clamd@scan/clamav-daemon, que té la seva pròpia configuració):

<table><tr>
<td> <b>--fdpass</b> </td><td> S'utilitza si el servidor escolta localment. Serveix per passar al servidor l'usuari (i els permissos associats) amb què s'ha executat la comanda clamdscan per tal de què el servidor utilitzi per fer l'escaneig aquest usuari i no pas el seu usuari per defecte (indicat a la directiva *User=* del seu arxiu de configuració, com veurem de seguida). Això sol ser necessari per a què el servidor pugui accedir a les mateixes carpetes/fitxers a les què pot accedir l'usuari que executa la comanda client.</td>
</tr><tr>
<td> <b>-l /ruta/fitxer.log</b> </td><td> Guarda en el fitxer indicat el resum estadístic de l'escaneig. </td>
</tr><tr>
<td> <b>-f /ruta/fitxer</b> </td><td> En lloc d'indicar directament el/s fitxer/s o carpeta/es a escanejar com a paràmetres de la comanda, es pot utilitzar aquest paràmetre, el qual serveix per indicar un fitxer el contingut del qual serà la llista de rutes de fitxers o carpetes que seran escanejades (una per línia). </td>
</tr></table>


### Arxius i directives de configuració importants en el mode de treball client/servidor

ClamAV, funcionant en el mode client-servidor, utilitza diversos fitxers de configuració que podrem editar segons les nostres necessitats (la comanda clamscan, del mode "standalone", en canvi, no té fitxer de configuració perquè funciona només a través dels paràmetres que se l'indiquin des de la línia de comandes). 

Concretament, el fitxer **"/etc/clamav/clamd.conf"** (a Ubuntu) o **"/etc/clamd.d/scan.conf"** (a Fedora) inclou directives relacionades amb el funcionament del dimoni clamd@scan/clamav-daemon. Alguna de les més destacables són les següents (pots trobar més informació a ```man clamd.conf```):

*    *DatabaseDirectory* : Indica la ruta de la carpeta on s'anirà a buscar la base de dades de signatures (ha de coincidir, lògicament, amb la ruta indicada a l'opció homònima a l'arxiu "freshclam.conf")
*    *ExcludePath* : Indica una expressió regular que concordarà amb els noms de fitxers o carpetes que no seran escanejades 
*    *MaxDirectoryRecursion* : Indica el número màxim de subcarpetes que s'escanejaran recursivament
*    *CrossFilesystems* : Indica si s'escanejaran (o no) carpetes o fitxers ubicats en altres sistemes de fitxers diferents de la ruta inicial indicada
*    *VirusEvent*: Indica la comanda que s'executarà automàticament quan es trobi un virus.  Aquí podem indicar, per exemple, l'execució d'alguna comanda per enviar emails o algun altre tipus d'alerta
*    *LocalSocket* : Indica el nom del "socket" (pot ser una cadena qualsevol) que s'emprarà per rebre peticions locals (si no s'indica aquesta directiva, el servei no escoltarà peticions locals)
*    *TCPSocket* : Indica el número de port que s'emprarà per rebre peticions remotes (si no s'indica aquesta directiva, el servei no escoltarà peticions provinents de la xarxa). Per defecte el servei es posa a escoltar en totes les adreces IP disponibles del sistema; si es vol restringir a una IP en concret, caldrà afegir la directiva *TCPAddr* (on s'hi haurà d'indicar l'adreça IP triada en qüestió)
*    *User* : Indica l'usuari amb els privilegis del qual el dimoni s'executarà. Per defecte aquest usuari és "clamscan"
*    *Example* : La presència d'aquesta línia fa que el dimoni clamd/clamav-daemon no s'inicïi

!!! info "Sobre el nom del fitxer de configuració del dimoni a Fedora"

    El nom (sense extensió) del fitxer de configuració del dimoni resident a Fedora ("scan.conf") ha de ser el mateix que el valor indicat després de l'arroba quan s'invoca dit dimoni (així, "clamd@scan"). Això és perquè el valor escrit després de l'arroba indica el nom del fitxer de configuració que farà servir el dimoni, de forma que es podria tenir diversos dimonis ClamAV funcionant a la vegada, cadascun amb una configuració diferent. 

D'altra banda, el fitxer que s'especifiqui amb el paràmetre **-c** de la comanda clamdscan bàsicament serveix per indicar, en el cas que el servidor clamd@scan/clamav-daemon funcioni de forma remota, l'adreça IP i port d'escolta d'aquest, per tal que el client clamdscan s'hi pugui connectar (si el servidor funciona de forma local aquest fitxer no és necessari). Concretament, les directives importants són:

*    *TCPAddr* : Indica l'adreça IP del servidor Clamd on s'hi connectarà
*    *TCPSocket* : Indica el nombre de port del servidor Clamd on s'hi connectarà


### Creació de signatures pròpies

#### Basades en hashes

La manera més fàcil de crear una signatura per ClamAV és utilitzant hashes de fitxers per fer comparacions estàtiques. No obstant, cal tenir en compte que aquest mètode no funcionarà tan bon punt un simple bit canvïi en el fitxer inspeccionat. La manera concreta seria tal com es proposa a continuació...:

1. Calcular el hash MD5 del fitxer en qüestió (suposarem que és l'executable */bin/ls*, per exemple) i guardar-lo en un arxiu de hashes (que anomenarem "prova.hdb", per exemple); en aquest arxiu hi podrien haver molts més hashes, cadascun a una línia diferent. Això es pot fer amb la comanda  ```sigtool --md5 /bin/ls > prova.hdb ```

2. Comprovar si ClamAV "detecta" la presència d'aquest hash dins de, per exemple, la carpeta */bin* del sistema, així: ```clamscan -d prova.hdb -i -r /bin```. Si no es vol haver d'especificar manualment l'arxiu ".hdb/.hsb" a utilitzar en l'escaneig, aquest arxiu es pot moure dins de la carpeta on està la base de dades oficial de signatures (és a dir, per defecte, "/var/lib/clamav") i serà utilitzat llavors com un fitxer més d'aquesta base de dades per defecte. 

o bé d'aquesta altra manera:

1. Calcular el hash SHA1 (o SHA256) del fitxer en qüestió (suposarem que és l'executable */bin/ls*, per exemple) i guardar-lo en un arxiu de hashes (que anomenarem "prova.hsb", per exemple). Això es pot fer amb la comanda ```sigtool --sha1 /bin/ls > prova.hsb``` (o ```sigtool --sha256 /bin/ls > prova.hsb```, respectivament).

2. Comprovar si ClamAV "detecta" la presència d'aquest hash dins de, per exemple, la carpeta */bin* del sistema, així: ```clamscan -d prova.hsb -i -r /bin``` Si no es vol haver d'especificar manualment l'arxiu ".hdb/.hsb" a utilitzar en l'escaneig, aquest arxiu es pot moure dins de la carpeta on està la base de dades oficial de signatures (és a dir, per defecte, "/var/lib/clamav") i serà utilitzat llavors com un fitxer més d'aquesta base de dades per defecte. 

!!! notice "Sobre els arxius 'cvd'"
    Els arxius ".cvd" no són més que contenidors comprimits (i signats) de múltiples fitxers ".hdb/.hsb/.mdb/.msb" (i de molts altres tipus que de seguida veurem, com .ndb, .ldb, etc). Aquests fitxers (per exemple, els inclosos dins de "main.cvd") es poden extreure a la carpeta on estiguem situats en aquell moment (i així poder-los consultar/manipular individualment, per exemple) si executem la comanda ```sigtool  -u /var/lib/clamav/main.cvd```. Per fer el procés contrari (és a dir, per crear un arxiu ".cvd" a partir d'un conjunt de fitxers de signatures individuals, cal executar una comanda similar a la següent dins de la carpeta mateixa on estan ubicats aquests fitxers de signatures:  ```sigtool --unsigned -b nomarxiu.cud```, on l'arxiu generat té l'extensió ".cud" en lloc de ".cvd" perquè no estarà signat per ClamAV (és per això que cal afegir també el paràmetre *--unsigned*)


#### Basades en el contingut de fitxers

El format més habitual de signatures, però, és el basat en seqüències de bytes (en hexadecimal) corresponents a part del contingut de fitxers. Aquestes seqüències admeten, a més, determinats símbols que tenen un significat especial com són:

| Símbol         | Equivalència                                                                       |
| -------------- | ---------------------------------------------------------------------------------- |
| ??             | Equival a un byte qualsevol                                                        |
| *              | Equival a qualsevol nombre de bytes                                                |
| ?a             | Equival a un byte on els seus quatre bits més significatius poden ser qualssevol   |
| a?             | Equival a un byte on els seus quatre bits menys significatius poden ser qualssevol |
| {n}            | Equival a n bytes qualssevol                                                       |
| {-n}           | Equival a n o menys bytes qualssevol                                               |
| {n-}           | Equival a n o més bytes qualssevol                                                 |
| {n-m}          | Equival a entre n i m bytes qualssevol                                             | 
| (aa\|bb\|cc\|..)  | Equival al byte "aa" o "bb" o "cc", etc                                            |
| !(aa\|bb\|cc\|..) | Equival a qualsevol byte excepte "aa" o "bb" o "cc", etc                           |
| (B)            | Equival a un límit de paraula (inclou final de fitxer)                             |
| (L)            | Equival a un salt de línia (CR, CRLF) o final de fitxer                            |
| (W)            | Equival a un caracter no alfanumèric                                               |


Tota signatura basada en seqüència de bytes s'ha d'escriure dins d'un arxiu ".ndb" en un format tal com el següent: *nommalware:tipustarget:offset:sequenciabytes* , on:

* *tipustarget* ha de ser algun dels següents valors, que serveixen per indicar el tipus de fitxer on s'espera trobar la signatura. Aquest valor serveix per evitar haver de fer la comprovació de signatures en cas que el fitxer a escanejar no sigui del tipus indicat en ella

| Valor | Significat                                                                                          |
| ----- | --------------------------------------------------------------------------------------------------- |
| 0     | Qualsevol fitxer (la signatura es comprova sempre, sigui quin sigui el tipus de fitxer a escanejar) |
| 1     | PE (Portable Executable):el format dels executables (*.exe, *.dll) de Windows                       |
| 3     | HTML normalitzat                                                                                    |
| 5     | Arxius gràfics                                                                                      |
| 6     | ELF (Executable Linux File): el format dels executables (*.so,...) de Linux                         |
| 7     | ASCII normalitzat                                                                                   |
| 9     | Mach-O: el format del executables de Mac                                                            |
| 10    | PDF                                                                                                 |

* *offset* ha de ser el nombre de bytes a comptar des del començament del fitxer on s'espera trobar el 	principi de la la seqüència indicada. També pot valer "*" per indicar "qualsevol" o el valor especial *EOF-nº* per especificar l'offset comptant des del final del fitxer en lloc de des del seu començament, entre altres valors més avançats (per indicar inici de seccions o de punts d'entrada en executables, etc). D'altra banda, també es pot especificar el valor *offset,maxshift* , on maxshift ha de ser un número 	sencer que serveix per indicar que la seqüència a buscar pot començar al byte ubicat a la posició *offset* , o bé a la posició *offset + 1 byte*, o la posició *offset + 2 bytes*, etc, fins arribar com a màxim a la posició *offset + maxshift bytes*.

A continuació mostrem pas a pas com es realitzaria una comprovació d'aquest tipus de signatures:

1. Crear un fitxer (podria ser binari o de text...en aquest exemple suposarem que és d'aquest darrer tipus i s'anomena "bitxo.txt") que contingui tan sols la següent frase (sense cometes): "Hola, em dic Pepe". L'objectiu serà crear una signatura basada en el contingut d'aquest fitxer de forma que si aquest contingut es trobés en qualsevol altre fitxer, es consideraria malware. Cal tenir en compte, però, que abans de generar la signatura del fitxer anterior, l'hem de "normalitzar". Aquest és un procediment que cal fer amb els fitxers que són de text pla (i també els que són HTML) i que consisteix en realitzar vàries transformacions (com ara convertir tots els caràcters a minúscules, treure els caràcters no imprimibles i col·lapsar els espais en blanc i salts de línia, entre altres accions); això és necessari perquè ClamAV només trobarà signatures si aquestes han sigut normalitzades. Per normalitzar el fitxer "bitxo.txt" cal que executis la següent comanda: ```sigtool --ascii-normalise bitxo.txt``` ; el resultat serà un nou fitxer (ubicat a la carpeta de treball) anomenat **"normalised_text"**

2. Obtenir la seqüència hexadecimal corresponent al contingut del fitxer "normalised_text" i guardar-la en un arxiu anomenat, per exemple "prova.ndb" (en aquest arxiu hi podrien haver moltes més seqüències, cadascuna a una línia diferent). Això es pot fer amb la comanda ```sigtool --hex-dump < normalised_text > prova.ndb``` Tot seguit es pot esborrar l'arxiu "normalised_text", no es necessitarà més.

3. Editar el contingut de l'arxiu "prova.ndb" per afegir-ne el prefix, per exemple, "Linux.Trojan.Pepe:7:0:" (sense cometes) a l'inici de la tira hexadecimal allà present. El valor més important és el 7, que indica el tipus de "target"; en aquest cas era molt evident, però si en alguna altra ocasió tinguéssim un fitxer per escanejar del qual no sapiguéssim el seu tipus (o millor dit, el tipus amb què ClamAV el reconeix) per tal de saber indicar el valor numèric d'aquest segon camp de la signatura, el que podem fer és escanejar-lo amb el mode "debug" activat per observar una línia que comenci per "Recognized", la qual indicarà el tipus de dada reconegut. És a dir, executar una comanda similar a ```clamscan --debug arxiudesconegut 2>&1 | grep -i "Recognized"```

4. Comprovar si ClamAV "detecta" la presència d'aquesta seqüència de bytes dins d'algun fitxer (suposarem, per exemple, ubicat dins de la teva carpeta personal), així: ```clamscan -d prova.ndb -i $HOME```


#### Lògiques

Més enllà de les signatures basades en seqüències de bytes (que anomenarem "body-based"), tenim la possibilitat d'utilitzar les anomenades signatures "lògiques", les quals permeten combinar múltiples signatures "body-based" mitjançant operadors lògics (bàsicament, AND i OR). Aquestes signatures lògiques s'han d'escriure dins d'un arxiu ".ldb" en un format tal com el següent: *nomsignatura;parellesdades; expressiologica;seqbytes0::mods;seqbytes1::mods;seqbytes2::mods;...* , on:

* *parellesdades* són parelles de tipus nom:valor (separades per comes) que aporten informació 	diversa sobre la signatura lògica en qüestió. D'entre tots els noms possibles, el que més sovint farem servir és *Target*, el qual pot tenir com a valor associat qualsevol dels números ja coneguts pels "targets" definits a ClamAV (0=qualsevol tipus de fitxer, 1=fitxers PE, 3=fitxers HTML, 5=fitxers gràfics, 6=fitxers ELF, etc), essent el significat el qual exactament el mateix que ja coneixíem: 	funcionar com a filtre per descartar la signatura lògica en el cas que el fitxers inspeccionat no concordi amb el valor "target" especificat.

* *expressiologica* descriu la relació lògica entre les seqüències de bytes indicades a continuació (és a dir, entre seqbytes0, seqbytes1, etc. Aquí cada seqüència s'identifica amb el número de posició en que apareixen dins de la signatura lògica en qüestió ("0", "1",...sense cometes) i les relacions lògiques poden ser les següents:

| Símbol         | Significat                                                                         |
| -------------- | ---------------------------------------------------------------------------------- |
| &              | És un "I" lògic. Per exemple, 0&1 indica que el fitxer inspeccionat en aquell moment, per a que sigui considerat malware, ha de contenir la seqüència de bytes nº0 i la seqüència nº1, no importa on. |
| \|             | És un "O" lògic. Per exemple, 0|1 indica que el fitxer inspeccionat en aquell moment, per a que sigui considerat malware, ha de contenir la seqüència de bytes nº0 o la seqüència nº1, no importa on. |
| A=X            | Indica que la seqüència especificada (aquí escrita com a "A" però que en realitat hauria de ser 0, 1, 2...) ha d'aparèixer dins del fitxer inspeccionat exactament el nombre de vegades X per a què sigui considerat malware, no importa on. En el cas concret que X valgués 0, indicaria que la signatura indicada no ha d'aparèixer per ser considerat el fitxer inspeccionat malware |
| A>X            | Indica que la seqüència especificada (aquí escrita com a "A" però que en realitat hauria de ser 0, 1, 2...) ha d'aparèixer dins del fitxer inspeccionat més vegades que el nombre X per a què sigui considerat malware, no importa on. |
| A<X            | Indica que la seqüència especificada (aquí escrita com a "A" però que en realitat hauria de ser 0, 1, 2...) ha d'aparèixer dins del fitxer inspeccionat menys vegades que el nombre X per a què sigui considerat malware, no importa on. |

* *seqbytesX* és la seqüència de bytes nºX tal qual, on es poden indicar igualment els mateixos símbols especials ja coneguts de les signatures "body-based" (??, *, {n}, (aa|bb|..), etc). També es pot indicar un "offset" específic per una determinada sequência indicant el seu valor abans de la pròpia seqüència (separant-lo d'aquesta per ":"), així: *offset:seqbytesX*

* *mods* representa un conjunt (opcional!) de modificadors associats a la seqüència de bytes precedent (i de la qual estan separats per "::"). Cadascun d'aquests modificadors té forma de lletra, i en podem destacar els següents:

| Modificador    | Significat                                                                         |
| -------------- | ---------------------------------------------------------------------------------- |
| i              | Indica que la confrontació entre el fitxer inspeccionat i la seqüència de bytes en qüestió, si aquesta es correspon amb símbols alfabètics, es faci de forma "case-insensitive" (és a dir, indistintament utilitzant majúscules i minúscules) |
| f              | Indica que la signatura en qüestió només serà considerada trobada si en el fitxer inspeccionat està delimitada entre dos caracters no alfanumèrics (com, per exemple, espais, tabuladors, salts de línia o altres símbols. La idea és que la signatura representi una paraula. |
|a               | Indica que la signatura en qüestió representa caràcters ASCII |
|w               | Indica que la signatura en qüestió representa caràcters codificats cadascun amb dos bytes |

Per exemple, si es busquen fitxers de qualsevol tipus que continguin tant "AAAA" com "BBB" (sense importar si són majúscules o minúscules, ni tampoc l'ordre en què apareguin al fitxer inspeccionat!), la signatura lògica a indicar podria ser: *Pepi1;Target:0;0&1;41414141::i;424242::i* (noteu com les signatures 0 i 1 indicades estan formades pels valors hexadecimals corresponents als caràcters buscats -"A" i "B", dins de la taula ASCII). Altres signatures lògiques més complexes són, per exemple, les següents:

Pepi2;Target:0;(0|1)&2&3;4141::i;4242::i;4343::i;4444::i 
Pepi3;Target:0;0&(1|2)&(3|4);4141::i;4242::i;4343::i;4444::i;4545::i 
Pepi4;Target:0;(0&1)|(1&2);4141::i;4242::i;4343::i 
Pepi5;Target:0;(0|1|2)>1;4141::i;4242::i;4343::i 
Pepi6;Target:0;((0&1&3)>2)&(2<3|2>5);4141::i;4242::i;4343::i;4444::i


### Exemples d'enunciats d'exercicis

**1.-** El fitxer EICAR és un fitxer (definit en un estàndard a nivell europeu) dissenyat per provar el comportament i resposta dels programes antivirus. La idea és permetre provar el funcionament del software antivirus sense haver d'utilitzar un vertader virus informàtic (el qual podria causar danys al sistema si l'antivirus no respongués correctament). Aquest fitxer (que conté només una cadena de text concreta) es pot descarregar directament d'[aquí:](https://secure.eicar.org/eicar.com.txt). Tant un antivirus amb protecció en temps real com mitjançant un escaneig hauria de detectar-lo immediatament, fins i tot si està dins d’un arxiu comprimit (sense contrasenya). Lògicament, per a què la prova funcioni, els programadors d’antivirus han d’establir la cadena d’EICAR com un virus verificat com qualsevol altra signatura. En tot cas, la prova EICAR, però, no serveix per saber quins virus s'és capaç de detectar o el nivell d'eficàcia de l'antivirus.

**a)** Després d'instal·lar els paquets de ClamAV adients a la teva distribució de treball, comprova que estigui iniciat el servei "clamav-freshclam" (i si no, inicia'l a mà). D'aquesta manera el sistema automàticament anirà comprovant de forma periòdica si hi ha disponibles actualitzacions de la base de dades de signatures llestes per descarregar

**b)** Descarrega't el fitxer EICAR de l'enllaç proporcionat (o crea'l a mà) i guarda'l dins de la carpeta "/opt" del sistema on tens instal·lat ClamAV. Fes servir la comanda clamscan adient per escanejar el contingut d'aquesta carpeta de forma recursiva. ¿L'escaneig detecta el fitxer EICAR?

<hr>

**2.-a)** Descomenta la línia *LocalSocket=* (i també les línies *LocalSocketGroup=* i *LocalSocketMode=* que l'acompanyen) del fitxer de configuració del servidor "clamd" instal·lat a la teva màquina de treball per tal de què el servei atengui peticions locals d'escaneig, i tot seguit posa en marxa aquest dimoni

**b)** Per a què no tinguis problemes de permisos a l'hora de connectar al socket local en executar clamdscan, el teu usuari de treball (suposem que s'anomena "usuari") ha de pertànyer al grup "virusgroup". Això ho pots aconseguir executant la comanda ```sudo usermod -a -G virusgroup usuari``` i reiniciant sessió. D'altra banda, en Fedora, executa la comanda ```sudo setsebool -P antivirus_can_scan_system  1``` per no tenir interferències amb el subsistema de seguretat integrada SELinux. Un cop fet tot això, dispara un escaneig a tot el contingut que hi hagi sota la carpeta "/opt" simplement executant la comanda *clamdscan --fdpass /opt* ¿Què passa?

<hr>

**3.-a)** Crea un arxiu de 100 bytes amb contingut binari aleatori executant, a la teva màquina virtual de treball, la comanda ```sudo dd if=/dev/urandom of=file.bin bs=1 count=100```. Tot seguit, instal·la-hi el paquet "hexedit" per tal de poder executar la comanda ```hexedit file.bin```. Consulta la seva pàgina del manual per saber com editar el contingut de quatre bytes qualssevol d'aquest fitxer per a què tinguin en conjunt el valor hexadecimal 41414141 i el d'altres tres bytes qualssevol per a què tinguin el valor hexadecimal 626262, i fes-ho

**b)** Crea un arxiu anomenat "prova.ldb" que contingui la signatura "Pepi1" indicada als paràgrafs de teoria anteriors i tot seguit executa ```clamscan -d prova.ldb -i file.bin``` ¿Què passa i per què?

**c)** Modifica la signatura "Pepi1" dins del fitxer "prova.ldb" per a què ara sigui així: *Pepi1;Target:0;0&1;25:41414141::i;5:626262::i* i torna a executar la comanda *clamscan* anterior. ¿Què passa ara i per què? Modifica adientment el fitxer "file.bin" per a què en tornar a executar-hi la mateixa comanda *clamscan* ara obtinguis un resultat diferent (tingues en compte que el 1r byte és el 0, no pas el 1!).

**d)** Modifica la signatura "Pepi1" dins del fitxer "prova.ldb" per a què ara sigui així: *Pepi1;Target:0;0&1;25,3:41414141::i;5,2:626262::i* i torna a executar la comanda *clamscan* anterior. ¿Què passa, i per què?

**e)** Sustitueix dins del fitxer "prova.ldb" la signatura "Pepi1" per "Pepi2" (també indicada a la teoria) i edita adientment el fitxer "file.bin" per a què en executar-hi la mateixa comanda *clamscan* es generi un positiu.

**f)** Sustitueix dins del fitxer "prova.ldb" la signatura "Pepi2" per "Pepi3" (també indicada a la teoria) i edita adientment el fitxer "file.bin" per a què en executar-hi la mateixa comanda *clamscan* es generi un positiu.

**g)** Sustitueix dins del fitxer "prova.ldb" la signatura "Pepi3" per "Pepi4" (també indicada a la teoria) i edita adientment el fitxer "file.bin" per a què en executar-hi la mateixa comanda *clamscan* es generi un positiu.

**h)** Sustitueix dins del fitxer "prova.ldb" la signatura "Pepi4" per "Pepi5" (també indicada a la teoria) i edita adientment el fitxer "file.bin" per a què en executar-hi la mateixa comanda *clamscan* es generi un positiu.

**i)** Sustitueix dins del fitxer "prova.ldb" la signatura "Pepi5" per "Pepi6" (també indicada a la teoria) i edita adientment el fitxer "file.bin" per a què en executar-hi la mateixa comanda *clamscan* es generi un positiu.

<hr>

**6.-a)** Afegeix una signatura nova al fitxer "prova.ldb" que comprovi si al fitxer inspeccionat apareix exactament 2 vegades la cadena "hola" a qualsevol ubicació (recorda que pots trobar la seva conversió a hexadecimal molt fàcilment executant ```echo -n "hola" | sigtool --hex-dump```) i més de 3 vegades, però només en els darrers 30 bytes del fitxer, la cadena "adeu". Edita adientment el fitxer "file.bin" per a què en executar-hi la mateixa comanda *clamscan* es generi un positiu.

**b)** Afegeix una nova cadena "hola" on vulguis en l'interior del fitxer "file.bin" i torna a passar la comanda *clamscan*. ¿Què passa i per què?

**c)** Desfés el que has fet a l'apartat anterior. Tot seguit, elimina una de les quatre cadenes "adeu" que hi havia al fitxer "file.bin" però escriu-la de nou al principi d'aquest fitxer. Torna a passar la comanda *clamscan*. ¿Què passa i per què?

**d)** Afegeix una signatura nova al fitxer "prova.ldb" que comprovi si el fitxer "file.bin" conté les cadenes "aa" i "bb" amb un mínim de tres bytes entre una i l'altra 

**e)** Afegeix una signatura nova al fitxer "prova.ldb" que comprovi si un fitxer PDF qualsevol (recordem que aquests fitxers tenen un "TargetType" igual a 10) conté dins el seu interior algun enllaç HTTP (és a dir, les cadenes "http://" o "https://")
