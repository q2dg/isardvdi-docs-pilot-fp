# Introducció


**Servei d'escriptoris virtuals per a centres públics de Formació Professional de Catalunya.**

El servei d'escriptoris virtuals basat en el programari [IsardVDI](https://isardvdi.com) és un projecte pilot que està donant servei a centres formatius de formació professional. De moment està disponible per a una sèrie de centres que han estat seleccionats i s'anirà ampliant de manera progressiva. Si hi ha dubtes de si un centre està donat d'alta en aquest servei, o vol demanar participar, pot enviar un correu a suport-gencatfp@isardvdi.com

Per a conèixer com s'utilitza el servei i les possibilitats que ofereix s'ha preparat aquest [vídeo de presentació de la plataforma](https://nextcloud.isardvdi.com/s/Ht5DZJALwHM586B)

El servei de suport inclou que es pugui enviar correus de qualsevol dubte o problema a suport-gencatfp@isardvdi.com.

Per al funcionament més detallat d'Isard es disposa del [manual de l'eina](https://isard.gitlab.io/isardvdi-docs/index.ca/). Aquí teniu un llistat amb accés directe als apartats destacats d'aquest manual pel professorat:

* [Visors](https://isard.gitlab.io/isardvdi-docs/user/viewers/viewers.ca/): Explicació dels tipus de visors avantatges i inconvenients

* [Desplegaments](https://isard.gitlab.io/isardvdi-docs/advanced/deployments.ca/): Desplegaments d'escriptoris com a professor

* [Plantilles](https://isard.gitlab.io/isardvdi-docs/advanced/templates.ca/): Creació de plantilles

Casos d'ús:

* [Instal·lació de Windows 10](https://isard.gitlab.io/isardvdi-docs/guests/win10/install.ca/): pas a pas com s'ha fet la plantilla de windows 10 per si es vol reproduir instal·lacions de windows
* [Instal·lació de Windows Server 2019 / 2022](https://isard.gitlab.io/isardvdi-docs/guests/win20192022/install.ca/): pas a pas com s'ha fet la plantilla de windows 2019 per si vols reproduir instal·lacions de windows
* [Xarxes privades i personals](https://isard.gitlab.io/isardvdi-docs/use_cases/private_and_personal_networks/private_and_personal_networks.ca/)
* [Exemple d'escriptori amb els diferents tipus de xarxes](https://isard.gitlab.io/isardvdi-docs/use_cases/network_tests/network_tests.ca/)

