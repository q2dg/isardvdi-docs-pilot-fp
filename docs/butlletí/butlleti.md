# Butlletins

Aquest apartat recopila tots els butlletins enviats per a oferir un accés ràpid i senzill a les nostres comunicacions passades. Explora el nostre historial de butlletins, organitzats cronològicament, i accedeix a informació valuosa i actualitzada que hem compartit amb la nostra audiència en cada enviament.

- [Buttletí #1: Editar desplegaments, editar usuaris d'un desplegament, roadmap, creixement de la plataforma, vídeos d'aprenentatge (20/10/2023)](%C3%9Altimes%20Novetats%20en%20IsardVDI!.html)
- [Butlletí #2: Paperera de reciclatge, canvi de propietari de dominis, botó reintentar (22/11/2023)](Butllet%C3%AD%20%232%20%C3%9Altimes%20Novetats%20a%20IsardVDI!.html)