# Monitoratge de consum

!!! warning "Rols amb accés"
    Només els **gestors (managers)** tenen accés a aquesta característica. Els gestors estan restringits a la seva pròpia categoria.

Aquest manual ha estat dissenyat per a proporcionar als usuaris **gestors** una visió completa sobre com supervisar i ajustar de manera efectiva l'ús de recursos en la plataforma IsardVDI.

En els pròxims apartats, explorarem com utilitzar les eines i característiques de monitoratge disponibles per a obtenir informació detallada sobre com s'estan utilitzant els recursos en el vostre entorn. Des del seguiment del consum de CPU i memòria fins a la gestió d'hores consumides per escriptori, aquest manual proporcionarà els coneixements necessaris per a prendre decisions informades i optimitzar la vostra configuració d'IsardVDI.

La nostra meta és proporcionar-vos les eines i els coneixements necessaris per a maximitzar l'eficiència i el rendiment del vostre entorn d'escriptoris virtuals.


Per a monitorar l'ús de la plataforma s'ha d'anar al panell d'Administració, es prem el botó ![](./usage.images/usage21.png)

![](./usage.images/usage22.png){width="90%"}

Es prem el botó ![](./usage.images/usage1.png)

![](./usage.images/usage2.png){width="40%"}

## Model de bosses

IsardVDI utilitza recursos com CPU, RAM i emmagatzematge en el núvol per proporcionar serveis d'escriptoris virtuals. És crucial tenir control sobre aquests recursos per garantir un rendiment òptim i una gestió eficaç.

Per a aconseguir això, hem desenvolupat un model de bosses que permetrà portar un seguiment més precís del consum de recursos i planificar millor les necessitats futures. 

**Cada centre tindrà assignada una bossa trimestral que es calcularà basat en l'ús que es fa de la plataforma**, i hem introduït uns nous paràmetres anomenat "hora d'escriptori estàndard" i "storage GB" que s'utilitzarà per a calcular el consum en funció de les especificacions dels escriptoris:

!!! info "La formula que vam seguir per fer aquests càlculs va ser aquesta:"

    === "DHN"
        **DHN = 0.2 * dsk_vcpus / 4 + 0.8 * dsk_mem / 6**

        * DHN = Desktop Hour Normalized (hora d’escriptori estàndard)
        * dsk_vcpus = vCPUs de l’escriptori per hora
        * dsk_mem = Memoria de l’escriptori per hora
    
    === "Storage GB"
        

Com es pot veure a la fórmula, el recurs que més afectaria, seria la memòria dels escriptoris, per això és important utilitzar una RAM adequada per cadascun dels escriptoris.

La nostra intenció és proporcionar un servei continu, sense interrupcions, però si sorgeix un consum no previst, treballarem junts per a analitzar cada cas i ajustar la bossa si és necessari, en consulta amb la direcció d'FP. Podeu enviar un correu a **suport-gencatfp@isardvdi.com** per fer les peticions.

**Si en algun moment el vostre centre supera els límits de consum, serem aquí per a col·laborar i trobar solucions adequades.**

!!! Warning "Gràfica de consum"
    * En aquest cas hem escollit ensenyar la gràfica de DHN per explicar certes limitacions, aquestes es mostraran en dibuixar-ne la gràfica:

    ![](./usage.images/usage23.png)

    Tipus de limitacions associades:

    * **Expected use area**: Zona calculada de consum mitjà. És a dir, l'ús que s'espera de la plataforma.
    * **Soft limit**: En arribar a aquesta limitació s'informarà l'usuari per tal que redueixi el consum de la plataforma.
    * **Hard limit**: En arribar a aquesta limitació s'informarà l'usuari i s'emetrà un avís al sistema indicant que s'ha arribat a aquest límit. L'usuari ha d'enviar un correu electrònic demanant, si és que vol, que s'augmenti el límit. Els responsables d'aquest tema ho avaluaran.

!!! Danger "Les limitacions únicament serveixen per comunicar als centres el fet de la superació del nivell actual, **en cap cas provocaran cap interrupció del servei**."

## Filtres disponibles

A l'apartat de "Grouping" teniu aquests filtres, DHN + storage, perquè les pugueu aplicar fàcilment a part d'altres filtres.

!!! info "Filtres "Grouping":"

    === "DHN"
        ![](./usage.images/usage24.png)
    === "Storage GB"
        ![](./usage.images/usage25.png)

## L'ús de la plataforma

!!! Info "Conceptes clau"
    * **Item type**:  Són els diferents tipus d'elements que poden consumir recursos dins del sistema, poden ser "desktop" (escriptori), "media" (mitjà), "storage" (emmagatzematge) i "user" (usuari).

    * **Parameter**: Cadascun dels diferents consums registrats. Per exemple quants escriptoris han arrencat, quantes hores han estat arrencats, quanta memòria han fet servir, quant emmagatzematge s'ha fet servir, etc.

La vista principal de l'apartat de monitoratge d'ús de la plataforma ofereix informació relativa al consum en un període determinat de temps. La columna **end** indica amb una fletxa com ha evolucionat (si ha augmentat o disminuït) aquest valor en el temps escollit. Es mostrarà el consum **acumulatiu**, és a dir, el **cosum realitzat des de l'inici del curs acadèmic**, per tant, sempre serà creixent.

![](./usage.images/usage3.png)

!!! Tip "Suggeriment"
    En comptes de visualitzar el consum des de l'inici del curs acadèmic, es pot saber concretament quant a augmentat/disminuït cada valor dins l'interval escollit. Es pot aconseguir de dues maneres:

    * Passant el ratolí per sobre la fletxa a la columna **end**:

    ![](./usage.images/usage6.png)

    * Seleccionant la casella ![](./usage.images/usage4.png):

    ![](./usage.images/usage5.png)

Es disposa de diversos filtres per ajustar la informació a les necessitats de l'usuari:

![](./usage.images/usage7.png)

* **Dates between**: Indica el període de temps a visualitzar.
* **Grouping**: Defineix l'agrupació de paràmetres a visualitzar.
!!! Warning "Atenció"
    * El grouping depèn de l'item type:

    ![](./usage.images/usage8.png)
* **Consumer**: Depenent del tipus d'element escollit es pot desglossar el consum de diferents maneres. És a dir:
    * D'un **escriptori** podem veure el consum per categoria, grup, desplegament, plantilla, usuari i individualment per cada escriptori.
    * Dels **mitjans**, **emmagatzematge** o **usuaris** podem veure el consum per categoria, grup i individualment per usuari.
* **Group**: Indica concretament l'element a visualitzar. En cas de no tenir cap valor escollit mostra la informació de tots els elements disponibles.

![](./usage.images/usage9.png)


## Generar gràfiques

Es poden seleccionar diversos elements marcant les caselles situades a l'esquerra i visualitzar-ne gràficament el consum prement el botó ![](./usage.images/usage10.png)

![](./usage.images/usage11.png)


Obrirà una finestra amb una gràfica on es visualitza l'evolució dels diferents paràmetres en l'interval de temps escollit. A més, sota la gràfica es disposa d'una taula amb la informació desglossada dia a dia. Es mostrarà el consum **acumulatiu**, és a dir, el **consum realitzat des de l'inici del curs acadèmic**, per tant, la gràfica sempre serà creixent:

![](./usage.images/usage12.png)

!!! Warning "Atenció"
    * El grouping seleccionat pot tenir associades certes limitacions, aquestes es mostraran en dibuixar-ne la gràfica:

    ![](./usage.images/usage20.png)

    Tipus de limitacions associades:

    * **Expected use area**: Zona calculada de consum mitjà. És a dir, l'ús que s'espera de la plataforma.
    * **Soft limit**: En arribar a aquesta limitació s'informarà l'usuari per tal que redueixi el consum de la plataforma.
    * **Hard limit**: En arribar a aquesta limitació s'informarà l'usuari i s'emetrà un avís al sistema indicant que s'ha arribat a aquest límit. L'usuari ha d'enviar un correu electrònic demanant, si és que vol, que s'augmenti el límit. Els responsables d'aquest tema ho avaluaran.

Es pot saber concretament quant a augmentat/disminuït cada paràmetre de manera diària fent clic a la casella ![](./usage.images/usage13.png)

![](./usage.images/usage14.png)

### Visualització de valors

Es pot visualitzar els valors dels diferents paràmetres en un punt concret a la gràfica passant el ratolí per sobre:

![](./usage.images/usage15.png)

Es poden seleccionar/deseleccionar els paràmetres a visualitzar a la vista de gràfica clicant a cadascun dels noms localitzats a la part superior de la gràfica:

![](./usage.images/usage16.png)

### Tipus de vista

Es pot canviar la vista de gràfica lineal a gràfica de barres clicant les icones corresponents:

![](./usage.images/usage17.png)

### Descàrrega

#### Imatge

Es pot descarregar la gràfica generada com a imatge:

![](./usage.images/usage18.png)

#### Taula

Es pot descarregar la taula amb la informació desglossada dia a dia:

![](./usage.images/usage19.png)